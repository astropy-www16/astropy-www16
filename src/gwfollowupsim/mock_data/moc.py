# Na podstawie przykładu z MOCpy:
#   https://cds-astro.github.io/mocpy/examples/examples.html#create-a-moc-from-a-concave-polygon

import numpy as np

from mocpy import MOC, World2ScreenMPL
from astropy.coordinates import Angle, SkyCoord
import astropy.units as u


def get_w_moc(save_fits=None):
    '''Letter-W-shaped MOC, inpired by the Warsztaty WWW logo.

    :type save_fits: str
    :param save_fits: a path to save the generated MOC to (default: None)
    
    :returns: the generated mocpy.MOC
    '''
    
    # The set of points delimiting the polygon in deg
    vertices = np.array([
        [15., 15.],
        [12., 15.],
        [9., 3.],
        [6., 15.],
        [3., 15.],
        [0., 3.],
        [-3., 15.],
        [-6., 15.],
        [-1.5, 0.],
        [1.5, 0.],
        [4.5, 12.],
        [7.5, 0.],
        [10.5, 0.]
    ])

    skycoord = SkyCoord(vertices, unit="deg", frame="icrs")
    # A point that we say it belongs to the inside of the MOC
    inside = SkyCoord(ra=0, dec=1.5, unit="deg", frame="icrs")
    moc = MOC.from_polygon_skycoord(skycoord, max_depth=9)
    
    if save_fits is not None:
        moc.write(save_fits, format='fits', overwrite=True)

    return moc


def plot_w_moc(show_plot=True, save_plot=None):
    '''Plot the letter-W-shaped MOC using matplotlib.
    
    :type show_plot: bool
    :param show_plot: if to show the plot (default: True)
    
    :type save_plot: str
    :param save_plot: a path to save the plot to (default: None)

    :returns: the generated matplotlib.pyplot.figure
    '''
    
    import matplotlib.pyplot as plt
    fig = plt.figure(111, figsize=(10, 10))

    moc = get_w_moc()
    
    # Define a astropy WCS easily
    with World2ScreenMPL(fig, 
            fov=25 * u.deg,
            center=SkyCoord(4.5, 7.5, unit='deg', frame='icrs'),
            coordsys="icrs",
            rotation=Angle(0, u.degree),
            # The gnomonic projection transforms great circles into straight lines. 
            projection="TAN") as wcs:
        ax = fig.add_subplot(1, 1, 1, projection=wcs)
        # Call fill with a matplotlib axe and the `~astropy.wcs.WCS` wcs object.
        moc.fill(ax=ax, wcs=wcs, alpha=0.5, fill=True, color="red", linewidth=1)
        moc.border(ax=ax, wcs=wcs, alpha=1, color="red")


    plt.xlabel('ra')
    plt.ylabel('dec')
    plt.title('"W" MOC from a polygon')
    plt.grid(color="black", linestyle="dotted")
    
    if show_plot:
        plt.show()

    if save_plot is not None:
        plt.savefig(save_plot)
    
    return fig

