from astroplan import Observer
from astropy.time import Time
import astropy.units as u


class ObservationStrategy:
    """
    A class used to create observation strategies

    __________
    Attributes:
        observers: list of astroplan.Observer objects

        observers_setup: 3-element list containing zenith and azimuth angle observers are facing and polar angle they are spanning on the sky

        observation_time: 2-element list of astropy.time.Time objects, defining starting and ending UTC time of observations
    """
    def __init__(self):
        self.observers = []
        self.observers_setup = None
        self.observation_time = None

    def add_observer(self, longitude, latitude, name=""):
        """
        Method adding observer to a strategy

        __________
        Parameters:
            longitude: Angle
            Longitude of geographical coordinates of an oberver
            
            latitude: Angle
            Latitude of geographical coordinates of an observer

            name: str, default ""
            Observer's name
        """
        self.observers.append(Observer(longitude=longitude, latitude=latitude, name=name))

    def set_observation_time(self, start, end):
        """
        Method defining timespan of the observations

        __________
        Parameters:
            start: string, format of "yyyy-mm-dd hh:mm:ss"
            Starting time of observations
        
            end; string, format of "yyyy-mm-dd hh:mm:ss"
            Ending time of observations
        """
        self.observation_time = [Time(start), Time(end)]

    def set_observer_setup(self, zen=0*u.deg, az=0*u.deg, span=30*u.deg):
        """
        Method defning observers' direction of facing and polar angle they are spanning on the sky

        __________
        Parameters:
            zen: Angle, default 0
            Zenith angle of observers' direction

            az: Angle, default 0
            Azimuth angle of observers' direction

            span: Angle, default 30
            Polar angle that observers' are spanning on the sky

        """
        self.observers_setup = [zen, az, span]

    def get_moc_sky_span(self):
        """
        Method for getting moc polygon spanned by all observers between given observation times
        (Method yet to be created)
        """
        pass


def get_sample_strategies():
    """
    Returns 2-element array of sample ObservationStartegy objects
    """
    strategy_1 = ObservationStrategy()
    strategy_1.add_observer(0*u.deg, 0*u.deg)
    strategy_1.add_observer(20*u.deg, 0*u.deg)
    strategy_1.add_observer(-20*u.deg, 0*u.deg)
    strategy_1.add_observer(40*u.deg, 0*u.deg)
    strategy_1.add_observer(-40*u.deg, 0*u.deg)
    strategy_1.set_observer_setup()
    strategy_1.set_observation_time("2020-03-16 20:00:00", "2020-03-16 23:59:00")

    strategy_2 = ObservationStrategy()
    strategy_2.add_observer(52.25*u.deg, 21*u.deg, "Warszawa")
    strategy_2.add_observer(55*u.deg, 37*u.deg, "Moskwa")
    strategy_2.set_observer_setup(zen=10*u.deg, az=90*u.deg, span=40*u.deg)
    strategy_2.set_observation_time("2019-03-02 00:00:00", "2020-03-02 02:00:00")
    return [strategy_1, strategy_2]
