from ..mock_data import moc


class MockSimulation():

    def __init__(self, strategy=None, event=None):
        '''We care about neither of the arguments here'''
        pass

    def get_sim_name(self):
        return 'MockSimulation'

    def get_observed_region(self):
        '''Import and return a mock MOC'''
        return moc.get_w_moc()

